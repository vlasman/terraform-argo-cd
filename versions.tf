terraform {
  required_version = ">= 1.3"

  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.33.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.2.0"
    }
  }
}
