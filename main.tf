# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace_v1
resource "kubernetes_namespace_v1" "main" {
  count = var.kubernetes_namespace_create ? 1 : 0

  metadata {
    name = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }
}

locals {
  private_repository = nonsensitive(var.git_credentials_url != "" && var.git_credentials_read_username != "" && var.git_credentials_read_token != "")
  admin_password     = coalesce(var.admin_password, try(one(random_password.admin_password[*].result)), "")
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password
resource "random_password" "admin_password" {
  count = var.admin_password == "" ? 1 : 0

  length  = 16
  special = false
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password
resource "random_password" "secret_key" {
  length  = 64
  special = true

  keepers = {
    # Generate a new secret key if the admin password changes
    admin_password = local.admin_password
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1
resource "kubernetes_secret_v1" "main" {
  depends_on = [
    kubernetes_namespace_v1.main,
  ]

  metadata {
    name      = "argocd-secret"
    namespace = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  data = merge({
    "admin.password"      = bcrypt(local.admin_password, 10) # See https://github.com/argoproj/argo-helm/blob/66638628/charts/argo-cd/values.yaml
    "admin.passwordMtime" = timestamp()
    "server.secretkey"    = random_password.secret_key.result
  }, var.extra_secrets)

  lifecycle {
    ignore_changes = [
      data["admin.password"],
      data["admin.passwordMtime"],
    ]

    replace_triggered_by = [
      # Recreate this Kubernetes secret if the admin password changes
      random_password.secret_key,
    ]
  }
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password
resource "random_id" "git_credentials_secret_name" {
  byte_length = 4
  prefix      = "argo-cd-git-credentials-"
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1
resource "kubernetes_secret_v1" "git_credentials" {
  depends_on = [
    kubernetes_namespace_v1.main,
  ]
  count = local.private_repository ? 1 : 0

  metadata {
    name      = random_id.git_credentials_secret_name.hex
    namespace = var.kubernetes_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repo-creds"
      "app.kubernetes.io/managed-by"   = "Terraform"
    }
  }

  data = {
    type     = "git"
    url      = var.git_credentials_url
    username = var.git_credentials_read_username
    password = var.git_credentials_read_token
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1#example-usage-service-account-token
resource "kubernetes_secret_v1" "install_service_account_token" {
  metadata {
    generate_name = "argo-cd-install-token"
    namespace     = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.install.metadata[0].name
    }
  }

  type = "kubernetes.io/service-account-token"
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account_v1
resource "kubernetes_service_account_v1" "install" {
  depends_on = [
    kubernetes_namespace_v1.main,
  ]

  metadata {
    name      = "argo-cd-install"
    namespace = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  lifecycle {
    ignore_changes = [
      secret,
    ]
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_v1
resource "kubernetes_cluster_role_v1" "install" {
  metadata {
    name = "${var.kubernetes_namespace}:argo-cd-install"
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["*"]
  }

  rule {
    non_resource_urls = ["*"]
    verbs             = ["*"]
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_binding_v1
resource "kubernetes_cluster_role_binding_v1" "install" {
  metadata {
    name = "${var.kubernetes_namespace}:argo-cd-install"
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.install.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.install.metadata[0].name
    namespace = kubernetes_service_account_v1.install.metadata[0].namespace
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1
resource "kubernetes_config_map_v1" "root_app_values" {
  metadata {
    name      = "argo-cd-values"
    namespace = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  data = {
    "values.yaml" = var.root_app_chart_values
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/resource
data "kubernetes_resource" "job_install" {
  api_version = "batch/v1"
  kind        = "Job"

  metadata {
    name      = kubernetes_job_v1.install.metadata[0].name
    namespace = kubernetes_job_v1.install.metadata[0].namespace
  }
}

# Instead of using the Terraform Helm provider, we use a job so we can fetch
# installation details from the argo-cd.application.yaml in the root app chart
# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/job_v1
resource "kubernetes_job_v1" "install" {
  depends_on = [
    kubernetes_namespace_v1.main,
    kubernetes_cluster_role_v1.install,
    kubernetes_cluster_role_binding_v1.install,
    kubernetes_secret_v1.main,
    kubernetes_secret_v1.git_credentials,
  ]

  wait_for_completion = true

  timeouts {
    create = "10m"
    update = "10m"
  }

  metadata {
    name      = "argo-cd-install"
    namespace = var.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  spec {
    backoff_limit = 0

    template {
      metadata {}

      spec {
        service_account_name = kubernetes_service_account_v1.install.metadata[0].name
        restart_policy       = "Never"

        container {
          name  = "install"
          image = "docker.io/library/alpine:3.16"
          command = [
            "ash",
            "-ce",
            <<-EOT
              ARCH=$(uname -m)
              case $ARCH in
                armv7*)  ARCH=arm;;
                aarch64) ARCH=arm64;;
                x86_64)  ARCH=amd64;;
                x86)     ARCH=386;;
                i686)    ARCH=386;;
                i386)    ARCH=386;;
                *) echo "Unsupported platform architecture: $ARCH" && exit 1;;
              esac
              case $ARCH in
                arm)
                  HELM_CHECKSUM=8b7ab762f813521d73e301f8402d2b48efea8115eb2870a7954e8d43f641f655
                  KUBECTL_CHECKSUM=49ab7f05bb27a710575c2d77982cbfb4a09247ec94a8e21af28a6e300b698a44
                  ;;
                arm64)
                  HELM_CHECKSUM=9f9da4a8dec486b9ccee8c391f13153ab8efc4d6173c6b43f05d7c5a0c613157
                  KUBECTL_CHECKSUM=a8e9cd3c6ca80b67091fc41bc7fe8e9f246835925c835823a08a20ed9bcea1ba
                  ;;
                amd64)
                  HELM_CHECKSUM=dafb9fb139e38e1633d2b07d3dd8005c618dc160338052b1e88f3390fd32cb3e
                  KUBECTL_CHECKSUM=e4e569249798a09f37e31b8b33571970fcfbdecdd99b1b81108adc93ca74b522
                  ;;
                386)
                  HELM_CHECKSUM=00cea13bef9663a88bbf14f554c11eb7cd418d1868fcb241149caf5a281ec32e
                  KUBECTL_CHECKSUM=4165634a86076a413f96cd9da20530336999f71fddb6805fcd3c740587cf28d2
                  ;;
              esac

              # Wait for the network
              until nslookup get.helm.sh;do
                sleep 5
              done

              wget -O /dev/stdout https://get.helm.sh/helm-v3.12.1-linux-$ARCH.tar.gz | tar -xz -C /usr/bin --strip-components=1 linux-$ARCH/helm
              sha256sum /usr/bin/helm
              echo "$HELM_CHECKSUM  /usr/bin/helm" | sha256sum -c

              wget -O /usr/bin/kubectl https://dl.k8s.io/release/v1.25.4/bin/linux/$ARCH/kubectl
              sha256sum /usr/bin/kubectl
              chmod +x /usr/bin/kubectl
              echo "$KUBECTL_CHECKSUM  /usr/bin/kubectl" | sha256sum -c

              apk add --no-cache git yq

              # Get and compile Argo CD application resource
              git clone $(echo "${var.root_app_repository_url}" | sed -E "s|(://)|\1$GIT_CREDENTIALS_USERNAME:$GIT_CREDENTIALS_PASSWORD@|") _root-app
              git -C _root-app checkout ${var.root_app_repository_ref}
              find _root-app/templates ! -name '*.tpl' ! -name 'argo-cd.application.yaml' -type f -delete
              helm template root-app ./_root-app \
                %{for item in local.root_app_chart_parameters~}
                --set="${item.name}"="${item.value}" \
                %{endfor~}
                --values=/mnt/root-app.values.yaml \
                --debug \
                > argo-cd.app.yaml

              cat argo-cd.app.yaml

              # Delete Argo CD before (re)installing
              helm delete --wait $(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml) || true
              kubectl delete configmap,cronjob,daemonset,deployment,ingress,job,persistentvolumeclaim,role,rolebinding,secret,service,serviceaccount,statefulset --selector=app.kubernetes.io/instance=$(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml)
              kubectl label customresourcedefinition --overwrite --selector=app.kubernetes.io/part-of=$(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml) app.kubernetes.io/managed-by=Helm
              kubectl annotate customresourcedefinition --overwrite --selector=app.kubernetes.io/part-of=$(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml) meta.helm.sh/release-name=$(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml)
              kubectl annotate customresourcedefinition --overwrite --selector=app.kubernetes.io/part-of=$(yq eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml) meta.helm.sh/release-namespace=${var.kubernetes_namespace}

              # Install Argo CD using the Argo CD application resource
              yq eval '.spec.source.helm | .valuesObject // .values' argo-cd.app.yaml > argo-cd.values.yaml
              eval $(echo helm upgrade --install $(yq -e eval '.spec.source.helm.releaseName // .metadata.name' argo-cd.app.yaml) $(yq -e eval '.spec.source.chart' argo-cd.app.yaml) \
                --repo=$(yq -e eval '.spec.source.repoURL' argo-cd.app.yaml) \
                --version=$(yq -e eval '.spec.source.targetRevision' argo-cd.app.yaml) \
                --namespace=$(yq -e eval '.spec.destination.namespace' argo-cd.app.yaml) \
                --create-namespace \
                --reset-values \
                --values=argo-cd.values.yaml \
                $(yq -e eval '.spec.source.helm.parameters[] | "--set=\"" + .name + "=" + .value + "\""' argo-cd.app.yaml | sed 's|,|\\\\\\\\\\,|g' | tr '\n' ' ') \
                --set=\"global.additionalLabels.argocd\\.argoproj\\.io/instance\"=\"$(yq -e eval '.metadata.name' argo-cd.app.yaml)\" \
                --wait \
                --wait-for-jobs \
                --force)
            EOT
          ]

          env {
            name = "GIT_CREDENTIALS_USERNAME"
            value_from {
              secret_key_ref {
                name     = length(kubernetes_secret_v1.git_credentials) != 0 ? kubernetes_secret_v1.git_credentials[0].metadata[0].name : "dummy"
                key      = "username"
                optional = length(kubernetes_secret_v1.git_credentials) == 0
              }
            }
          }

          env {
            name = "GIT_CREDENTIALS_PASSWORD"
            value_from {
              secret_key_ref {
                name     = length(kubernetes_secret_v1.git_credentials) != 0 ? kubernetes_secret_v1.git_credentials[0].metadata[0].name : "dummy"
                key      = "password"
                optional = length(kubernetes_secret_v1.git_credentials) == 0
              }
            }
          }
          volume_mount {
            name       = "root-app-values"
            sub_path   = "values.yaml"
            mount_path = "/mnt/root-app.values.yaml"
          }
        }

        volume {
          name = "root-app-values"
          config_map {
            name = kubernetes_config_map_v1.root_app_values.metadata[0].name
          }
        }
      }
    }
  }
}

locals {
  root_app_chart_parameters = concat([
    {
      name  = "namespace"
      value = var.kubernetes_namespace
    },
  ], var.root_app_chart_parameters)
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
resource "kubernetes_manifest" "root_app" {
  depends_on = [
    kubernetes_namespace_v1.main,
  ]

  count = var.kubernetes_custom_resources_enabled ? 1 : 0

  manifest = {
    apiVersion = var.root_app_crd_api_version
    kind       = "Application"
    metadata = {
      name      = "root"
      namespace = var.kubernetes_namespace
      labels = {
        "app.kubernetes.io/managed-by" = "Terraform"
      }
      finalizers = [
        "resources-finalizer.argocd.argoproj.io",
      ]
    }
    spec = {
      project = "default"
      destination = {
        namespace = var.kubernetes_namespace
        server    = "https://kubernetes.default.svc"
      }
      source = {
        repoURL        = var.root_app_repository_url
        path           = "."
        targetRevision = var.root_app_repository_ref
        helm = {
          parameters = local.root_app_chart_parameters
          values     = var.root_app_chart_values
        }
      }
      syncPolicy = {
        automated = {
          prune    = true
          selfHeal = true
        }
      }
    }
  }

  wait {
    fields = {
      "status.sync.status" = "Synced"
    }
  }

  timeouts {
    create = "30m"
    delete = "30m"
  }

  field_manager {
    force_conflicts = true
  }

  computed_fields = [
    "spec.source.targetRevision",
    "spec.syncPolicy",
  ]
}
