# Method 3: Apply on existing cluster

**This method is recommended if you already have a running Kubernetes cluster not managed by Terraform.**

## Installation

1. Clone this repository.

   ```sh
   git clone https://gitlab.com/vlasman/terraform-argo-cd.git
   ```

1. Ensure the required prerequisites are installed.

   | executable  | download link                                   |
   | ----------- | ----------------------------------------------- |
   | `terraform` | https://www.terraform.io/downloads              |
   | `kubectl`   | https://kubernetes.io/docs/tasks/tools/#kubectl |

1. Set the Kubernetes config environment variable.

   To connect Terraform to your cluster export the `KUBE_CONFIG_PATH` environment variable.

   ```sh
   export KUBE_CONFIG_PATH="<your kubeconfig file>"
   ```

1. Create a file named `terraform.tfvars' to configure your settings.

   ```terraform
   ## Optionally provide a new non-existing custom namespace.
   # kubernetes_namespace = "argo-system"

   ## Configuring this block is only necessary if your root app repository is private
   ## and could also be GitHub or something else.
   # git_credentials_url           = "https://gitlab.com/<your username>"
   # git_credentials_read_username = "<your username>"
   # git_credentials_read_token    = "<your api token>"

   ## Connect your root app chart repository. You can fork from https://gitlab.com/vlasman/root-app-chart.
   root_app_repository_url = "<your root app chart repository>"
   root_app_repository_ref = "HEAD"
   ## Optionally provide Helm chart parameters and/or values to pass to your root app
   # root_app_chart_parameters = [
   #   {
   #     name = "baseDomainName"
   #     value = "yourdomain.tld"
   #   }
   # ]
   # root_app_chart_values = <<-EOT
   #   baseDomainName: yourdomain.tld
   # EOT
   ```

1. Provision the module.

   The Kubernetes Custom Resource Definitions are initially not available so first we need to create a plan without the CRD-dependend resources. In the second apply they should be available and all resources can be applied.

   ```sh
   # Only needed for the initial apply
   terraform apply -var=kubernetes_custom_resources_enabled=false
   ```

   ```sh
   terraform apply
   ```

1. If the provisioning is completed test if Argo CD is installed.

   ```sh
   kubectl get pods --all-namespaces
   ```

   You should see a list of pods including the ones from Argo CD.

   ```sh
   kubectl get apps --all-namespaces
   ```

   You should see the 'root' Argo CD application.
