You can use this module in multiple ways.

| Description                                                             | Rationale                                                                  |
| ----------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| Method 1: [Install with new Kubernetes cluster][m1] ([quick-start][ib]) | If you want to get started quickly.                                        |
| Method 2: [Include as Terraform module][m2]                             | If you already have a Terraform setup or need additional functionality.    |
| Method 3: [Apply on an exising Kubernetes cluster][m3]                  | If you already have a running Kubernetes cluster not managed by Terraform. |

[m1]: docs/install-with-new-kubernetes-cluster.md
[m2]: docs/include-as-terraform-module.md
[m3]: docs/apply-on-existing-cluster.md
[ib]: https://gitlab.com/vlasman/infra-bootstrap
