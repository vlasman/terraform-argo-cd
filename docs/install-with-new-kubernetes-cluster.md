# Method 1: Install with new Kubernetes cluster

**This is the easiest method and recommended if you want to get started quickly.**

Create a new Kubernetes cluster using https://gitlab.com/vlasman/infra-bootstrap where this module is integrated. Follow the instructions in its repository.
