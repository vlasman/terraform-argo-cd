output "install_completed" {
  description = "Wether the Terraform module install is complete"
  value       = data.kubernetes_resource.job_install.object.metadata.name != null && var.kubernetes_custom_resources_enabled
}

output "kubernetes_crds_available" {
  description = "Wether the Kubernetes Custom Resource Definitions are available"
  # Unfortunatly it is not yet possible to get the status: https://github.com/hashicorp/terraform-provider-kubernetes/issues/1699.
  # The actual job object is only available when it exists in Kubernetes so using that for now.
  value = data.kubernetes_resource.job_install.object.metadata.name != null
}

output "kubernetes_namespace" {
  description = "The Kubernetes namespace Argo CD resources are deployed to"
  value       = var.kubernetes_namespace
}

output "root_app_repository_url" {
  description = "The repository URL of the root app"
  value       = var.root_app_repository_url
}

output "root_app_repository_path" {
  description = "The repository path of the root app"
  value       = regex("^https?:\\/\\/[^\\/]*\\/(.+?)(?:\\.git$|$)", var.root_app_repository_url)[0]
}

output "admin_password" {
  description = "The provided/generated Argo CD admin password"
  sensitive   = true
  value       = local.admin_password
}
